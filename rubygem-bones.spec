# Generated from bones-2.4.2.gem by gem2rpm -*- rpm-spec -*-
%define ruby_sitelib %(ruby -rrbconfig -e "puts Config::CONFIG['sitelibdir']")
%define gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%define gemname bones
%define geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: Mr Bones is a handy tool that builds a skeleton for your new Ruby projects
Name: rubygem-%{gemname}
Version: 2.4.2
Release: 1%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://codeforpeople.rubyforge.org/bones
Source0: http://gems.rubyforge.org/gems/%{gemname}-%{version}.gem
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: rubygems
Requires: rubygem(rake) >= 0.8.3
BuildRequires: rubygems
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}

%description
Mr Bones is a handy tool that builds a skeleton for your new Ruby projects.
The skeleton contains some starter code and a collection of rake tasks to ease
the management and deployment of your source code. Mr Bones is not viral --
all the code your project needs is included in the skeleton (no gem dependency
required).


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}
mkdir -p %{buildroot}/%{_bindir}
mv %{buildroot}%{gemdir}/bin/* %{buildroot}/%{_bindir}
rmdir %{buildroot}%{gemdir}/bin
find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{_bindir}/bones
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/History.txt
%doc %{geminstdir}/README.rdoc
%doc %{geminstdir}/bin/bones
%doc %{geminstdir}/lib/bones/tasks/ann.rake
%doc %{geminstdir}/lib/bones/tasks/bones.rake
%doc %{geminstdir}/lib/bones/tasks/gem.rake
%doc %{geminstdir}/lib/bones/tasks/git.rake
%doc %{geminstdir}/lib/bones/tasks/notes.rake
%doc %{geminstdir}/lib/bones/tasks/post_load.rake
%doc %{geminstdir}/lib/bones/tasks/rdoc.rake
%doc %{geminstdir}/lib/bones/tasks/rubyforge.rake
%doc %{geminstdir}/lib/bones/tasks/spec.rake
%doc %{geminstdir}/lib/bones/tasks/svn.rake
%doc %{geminstdir}/lib/bones/tasks/test.rake
%doc %{geminstdir}/spec/data/foo/README.txt
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Wed Apr 01 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 2.4.2-1
- Initial package
